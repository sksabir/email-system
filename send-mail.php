<?php

require_once __DIR__ . '/vendor/autoload.php';

require 'config.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = $config['host'];  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = $config['username'];                 // SMTP username
    $mail->Password = $config['password'];                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom($config['sender'], $config['sender_name']);
    $receivers = explode(",",$argv[2]);
    foreach ($receivers as $receiver){
        $mail->addAddress($receiver);
    }

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Alert From AWS Monitoring';
    $mail->Body = $argv[1];

    $mail->send();
    echo 'Message has been sent';


    $response = new \React\Http\Response(
        200,
        array(
            'Content-Type' => 'application/json',
        ),
        json_encode(["status" => 200,
            "data" => "success"
        ])
    );

    return $response;


} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}