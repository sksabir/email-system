<?php
require_once __DIR__ . '/vendor/autoload.php';

require 'config.php';
use React\EventLoop\Factory;
use React\Http\Server;

$loop = Factory::create();
$server = new Server(function (Psr\Http\Message\ServerRequestInterface $request) {

    $posts = $request->getParsedBody();
    if(empty($posts['body']) || empty($posts['receivers'])){
        return new \React\Http\Response(
            200,
            array(
                'Content-Type' => 'application/json',
            ),
            json_encode(["status" => 'error',
                "message" => 'Parameter body or receivers missing'
            ])
        );
    }

    $cmd = "php send-mail.php '" .
        escapeshellcmd($posts['body']) .
        "' '" .
        escapeshellcmd($posts['receivers']) .
        "' > /dev/null 2>&1 &";

    exec($cmd . ' > /dev/null 2>&1 &');

    return new \React\Http\Response(
        200,
        array(
            'Content-Type' => 'application/json',
        ),
        json_encode(["status" => 'success',
            "message" => "sent"
        ])
    );

});

$socket = new React\Socket\Server($config['SERVER_PORT'], $loop);
$server->listen($socket);
$loop->run();